import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import * as INSTRUCTORS_LIST_CONFIG from './instructors-table.config.list.json';
import { AdvancedRowActionComponent, AdvancedTableComponent, AdvancedTableDataResult } from '@universis/ngx-tables';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivatedTableService } from '@universis/ngx-tables';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { Observable, Subscription } from 'rxjs';
import { ConfigurationService, ErrorService, LoadingService, ModalService, UserActivityService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import { ElotConverter } from '@universis/elot-converter';
import { ClientDataQueryable } from '@themost/client';

@Component({
  selector: 'app-instructors-table',
  templateUrl: './instructors-table.component.html'
})
export class InstructorsTableComponent implements OnInit, OnDestroy {

  public readonly config = INSTRUCTORS_LIST_CONFIG;
  public recordsTotal: any;
  public defaultLocale: string;
  private dataSubscription: Subscription;
  private selectedItems = [];
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _loadingService: LoadingService,
    private _modalService: ModalService,
    private _errorService: ErrorService,
    private _context: AngularDataContext,
    private _configurationService: ConfigurationService
  ) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }
      this.defaultLocale = this._configurationService.settings
        && this._configurationService.settings.i18n
        && this._configurationService.settings.i18n.defaultLocale;
      this._userActivityService.setItem({
        category: this._translateService.instant('Instructors.Title'),
        description: this._translateService.instant(this.table.config.title),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });

  }

  async convertToElot() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItemsWithLocales();
      const elotMap = new Map<number, {familyName: string, givenName: string, enLocale: any}>();
      // validate familyName, givenName
      this.selectedItems = items.filter(instructor => {
        const convertedFamilyName = ElotConverter.convert(instructor.familyName);
        const convertedGivenName = ElotConverter.convert(instructor.givenName);
        // try to find en locale
        const enLocale = instructor.locales.find(locale => locale.inLanguage === 'en');
        // add to map to save conversion for later
        elotMap.set(instructor.id, {familyName: convertedFamilyName, givenName: convertedGivenName, enLocale: enLocale});
        if (enLocale) {
          return (enLocale.familyName !== convertedFamilyName || enLocale.givenName !== convertedGivenName);
        }
        return instructor;
      });
      this._loadingService.hideLoading();
      // open modal
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Instructors.ConvertToElotAction.Title',
          description: 'Instructors.ConvertToElotAction.Description',
          errorMessage: 'Instructors.ConvertToElotAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeConvertToElotAction(elotMap)
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeConvertToElotAction(elotMap: Map<number, {familyName: string, givenName: string, enLocale: any}>) {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // get converted values from map
            const convertedValues = elotMap.get(item.id);
            // update locales
            if (convertedValues.enLocale) {
              // if locale exists, update it
              Object.assign(convertedValues.enLocale, {
                familyName: convertedValues.familyName,
                givenName: convertedValues.givenName
              });
            } else {
              if (Array.isArray(item.locales)) {
                // create en locale
                item.locales.push({
                  inLanguage: 'en',
                  familyName: convertedValues.familyName,
                  givenName: convertedValues.givenName
                });
              }
            }
            // and save
            await this._context.model('Instructors').save(item);
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItemsWithLocales() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'familyName', 'givenName', 'locales'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .expand('locales')
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          const itemPromises = this.table.selected.map(async item => await this._context.model('Instructors')
            .where('id').equal(item.id).select('id, familyName, givenName, locales').expand('locales').getItem());
          items = await Promise.all(itemPromises);
        }
      }
    }
    return items;
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
