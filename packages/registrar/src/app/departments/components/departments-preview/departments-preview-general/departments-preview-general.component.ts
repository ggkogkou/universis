import { Component, OnInit, Input } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-departments-preview-general',
  templateUrl: './departments-preview-general.component.html'
})
export class DepartmentsPreviewGeneralComponent implements OnInit {

  @Input() department: any;
  
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {

    // this.department = await this._context.model('StudentPeriodRegistrations/' + this._activatedRoute.snapshot.params.id)
    // .asQueryable()
    // .select('*')
    // .getItem();

    this.department = await this._context.model('LocalDepartments')
    .where('id').equal(this._activatedRoute.snapshot.params.id)
    .getItem();
  }

}
