import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-graduations-overview-documents',
  templateUrl: './graduations-overview-documents.component.html'
})
export class GraduationsOverviewDocumentsComponent implements OnInit {

  @Input() public graduationEvent: any;

  constructor() { }

  ngOnInit() {

  }

}
