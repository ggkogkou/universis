import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-courses-overview-form',
  templateUrl: './courses-overview-form.component.html'
})
export class CoursesOverviewFormComponent implements OnInit {

  // courseId is optional, if model is empty
  @Input() courseId: any;
  @Input() model: any;
  @Input() showEdit = true;
  @Input() parentCourse: any;
  /* an optional input, used to hide the "more" anchor when the course tab is already being displayed
     e.g. when this *reusable* component is created by the courses dashboard module. */
  @Input() sourceTab?: string;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    if (!this.model) {
      if (this.courseId) {
        this.model = await this._context.model('Courses')
          .where('id').equal(this.courseId)
          .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory')
          .getItem();
      }
    }
    if (typeof this.sourceTab === 'undefined') {
      this.sourceTab = 'notCoursesTab';
    }
  }


}
