import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-courses-overview-general',
  templateUrl: './courses-overview-general.component.html',
  styleUrls: ['./courses-overview-general.component.scss']
})
export class CoursesOverviewGeneralComponent implements OnInit, OnDestroy {
  public model: any;
  public courseId: any;
  private subscription: Subscription;
  private fragmentSubscription: Subscription;
  public parentCourse: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseId = params.id;
      this.fetchCourses();
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.fetchCourses();
        }
      });

    });
  }

  async fetchCourses() {
    this.model = await this._context.model('Courses')
    .where('id').equal(this.courseId)
    .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory')
    .getItem();
    if (this.model.courseStructureType.id === 8) {
      this.parentCourse = await this._context.model('Courses')
        .where('id').equal(this.model.parentCourse)
        .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory')
        .getItem();
    }
    // if replacedByCourse exists
    if (this.model.replacedByCourse) {
      // get course
      const replacedByCourse = await this._context.model('Courses')
          .where('id').equal(this.model.replacedByCourse)
          .select('id, displayCode, name')
          .getItem();
      // override model field
      this.model.replacedByCourse = replacedByCourse;
    }
    // if replacedCourse exists
    if (this.model.replacedCourse) {
      // get course ids
      const replacedCoursesIds = this.model.replacedCourse.split(',').map(id => id.trim());
      // define promise chain
      const courses = replacedCoursesIds.map(async courseId => {
        return await this._context.model('Courses').where('id').equal(courseId).select('id, displayCode, name').getItem();
      });
      // execute promises and get courses
      const replacedCourseUpdated = await Promise.all(courses);
      // override model field
      this.model.replacedCourse = replacedCourseUpdated;
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }
}
